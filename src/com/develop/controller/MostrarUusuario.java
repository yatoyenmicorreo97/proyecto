package com.develop.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develo.model.UusuarioM;
import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class MostrarUusuario
 */
@WebServlet("/MostrarUusuario")
public class MostrarUusuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MostrarUusuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		System.out.println("estamos en doGet");
		RequestDispatcher rd;
		
		UsuarioDAO user = new UsuarioDAO();
		ArrayList<UusuarioM> cont = user.listarUusuario();
		String listaU = "";
		if(user!= null) {
			System.out.println("se encuentra algo en el contenedor");
			rd = request.getRequestDispatcher("/vistasUsuario/mostrarUsuarios.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("no hay nada en el contenedor");
			rd = request.getRequestDispatcher("/vistasUsuario/usuarios.jsp");
			rd.forward(request, response);
		}
		//<button type="delete">eliminar</button><button type="update">actualizar</button>
		
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		System.out.println("estamos en dopost");
	}

}
