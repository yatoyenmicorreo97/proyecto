package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develo.model.UusuarioM;
import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class UsuarioC
 */
@WebServlet("/UsuarioC")
public class UsuarioC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		RequestDispatcher rq;
		String usuario;
		String password;
		
		usuario = request.getParameter("usuario");
		password = request.getParameter("password");
		System.out.println(usuario +"  "+ password);
		
		UsuarioDAO dao = new UsuarioDAO();//se instancia para poder hacer uso del metodo
		
		//UusuarioM per= null;
		
		String respuesta = dao.validaUsuaurio(usuario, password);//argumentos que se van a pasar
		
		if(respuesta.equals("ok")) {
			System.out.println("Redireccionado");
			
			rq = request.getRequestDispatcher("/vistas/main.jsp");
			rq.forward(request, response);
			
		}else {
			System.out.println("No valido");
			
			rq = request.getRequestDispatcher("/vistas/login.jsp");
			rq.forward(request, response);
		}
		
	}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		
		
	}

}
