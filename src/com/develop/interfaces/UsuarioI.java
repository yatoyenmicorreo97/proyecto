package com.develop.interfaces;

import java.util.ArrayList;
import java.util.List;

import com.develo.model.UusuarioM;

public interface UsuarioI {
	
	public String validaUsuaurio(String nombre, String password);
		
	public String insertarUsuario(String nombre, String usuario, String password);
	
	public ArrayList<UusuarioM> listarUusuario();
	
	public String eliminarUsuario();
	
	


}
